package client;

import client.service.ClientService;
import client.service.MovieService;
import client.service.RentalService;
import client.ui.Console;
import common.service.IClientService;
import common.service.IMovieService;
import common.service.IRentalService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainClient {
    public static void main(String[] args) {
        System.out.println("Server boot up");
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        TcpClient tcpClient = new TcpClient();
        IClientService clientService = new ClientService(executorService, tcpClient);
        IMovieService movieService = new MovieService(executorService, tcpClient);
        IRentalService rentalService = new RentalService(executorService, tcpClient);
        Console console = new Console(clientService, movieService, rentalService);
        console.run();

        executorService.shutdown();

        System.out.println("Client shut down");
    }
}
