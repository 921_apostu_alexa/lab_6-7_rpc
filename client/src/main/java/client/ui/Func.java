package client.ui;

import java.util.concurrent.ExecutionException;

public interface Func {
    void exec() throws ExecutionException, InterruptedException;
}
