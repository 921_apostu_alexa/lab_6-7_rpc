package client.ui;

import client.service.ClientService;
import client.service.MovieService;
import client.service.RentalService;
import common.model.Client;
import common.model.Movie;
import common.model.Rental;
import common.service.IClientService;
import common.service.IMovieService;
import common.service.IRentalService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;


public class Console {
    private IClientService clientService;
    private IMovieService movieService;
    private IRentalService rentalService;
    private Menu menu;
    private boolean running = true;

    public Console(IClientService clientService, IMovieService movieService, IRentalService rentalService) {
        this.clientService = clientService;
        this.movieService = movieService;
        this.rentalService = rentalService;
        this.initialiseMenu();
    }

    public void initialiseMenu() {
        this.menu = new Menu();
        this.menu.addCommand("c1", "Add client", this::addClient);
        this.menu.addCommand("c2", "Print all clients", this::printClients);
        this.menu.addCommand("c3", "Filter clients by name", this::filterClients);
        this.menu.addCommand("c4", "Update client", this::updateClient);
        this.menu.addCommand("c5", "Remove client", this::removeClient);
        this.menu.addCommand("", "", null);
        this.menu.addCommand("m1", "Add movie", this::addMovie);;
        this.menu.addCommand("m2", "Print all movies", this::printMovies);
        this.menu.addCommand("m3", "Filter movies by year", this::filterMovies);
        this.menu.addCommand("m4", "Update movie", this::updateMovie);
        this.menu.addCommand("m5", "Remove movie", this::removeMovie);
        this.menu.addCommand("", "", null);
        this.menu.addCommand("r1", "Add rental", this::addRental);
        this.menu.addCommand("r2", "Print all rentals", this::printRentals);
        this.menu.addCommand("r3", "Print the most rented movie", this::printMostRentedMovie);
        this.menu.addCommand("r4", "Update rental", this::updateRental);
        this.menu.addCommand("r5", "Remove rental", this::removeRental);
        this.menu.addCommand("", "", null);
        this.menu.addCommand("x", "Exit",() -> {this.running = false;} );
    }

    public void run() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(this.menu.getMenu());
        try {
            System.out.println("\n>");
            String choice = buffer.readLine();
            this.menu.run(choice);

            if (this.running)
                this.run();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    // ---- Helpers ---- //

    private Client readClient() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter client name: ");
            String name = buffer.readLine();

            System.out.println("Enter client email: ");
            String email = buffer.readLine();

            return new Client(name, email);

        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        return null;
    }

    private Movie readMovie() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter movie title: ");
            String title = buffer.readLine();

            System.out.println("Enter movie duration (in minutes): ");
            int minutes = Integer.parseInt(buffer.readLine());

            System.out.println("Enter movie year: ");
            int year = Integer.parseInt(buffer.readLine());

            return new Movie(title, minutes, year);

        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        return null;
    }

    // ---- Clients ---- //

    public void addClient() throws ExecutionException, InterruptedException {
        System.out.println(" -- Adding client -- ");
        Client client = readClient();
        boolean wasAdded = this.clientService.addClient(client).get();
        if (wasAdded)
            System.out.println("Client with id: " + client.getId() + " was added");
        else
            System.out.println("Error, please try again later");
    }

    public void printClients() throws ExecutionException, InterruptedException {
        System.out.println(" << Clients >> ");
        this.clientService.getClients().get().stream().sorted()
                .forEach(System.out::println);
        System.out.println();
    }

    public void filterClients() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" << Filtering clients >> ");
            System.out.println("Enter client name: ");
            String name = buffer.readLine();

            this.clientService.filterClientsByName(name).get().forEach(System.out::println);

        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void updateClient() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Updating client -- ");

            System.out.println("Enter client id:");
            Long id = Long.parseLong(buffer.readLine());

            System.out.println("Enter client name: ");
            String name = buffer.readLine();

            System.out.println("Enter client email: ");
            String email = buffer.readLine();

            this.clientService.updateClient(new Client(id, name, email));

        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    public void removeClient() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" << Removing client >> ");
            System.out.println("Enter client id: ");
            Long id = Long.parseLong(buffer.readLine());

            this.clientService.removeClient(id);
            this.rentalService.removeRentalsWithClientID(id);

        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }
//
//    // ---- Movies ---- //
//
    public void addMovie() {
        Movie movie = readMovie();
        this.movieService.addMovie(movie);
    }

    public void printMovies() throws ExecutionException, InterruptedException {
        System.out.println(" << Movies >> ");
        this.movieService.getMovies().get().stream().sorted()
                .forEach(System.out::println);
        System.out.println();
    }

    public void filterMovies() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Filtering movies -- ");
            System.out.println("Enter movie year: ");
            int year = Integer.parseInt(buffer.readLine());

            this.movieService.filterMoviesByYear(year).get().forEach(System.out::println);

        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void updateMovie() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Updating movie -- ");

            System.out.println("Enter movie id:");
            Long id = Long.parseLong(buffer.readLine());

            System.out.println("Enter movie title: ");
            String title = buffer.readLine();

            System.out.println("Enter movie duration (in minutes): ");
            int minutes = Integer.parseInt(buffer.readLine());

            System.out.println("Enter movie year: ");
            int year = Integer.parseInt(buffer.readLine());

            this.movieService.updateMovie(new Movie(id, title, minutes, year));

        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    public void removeMovie() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Removing movie -- ");
            System.out.println("Enter movie id: ");
            Long id = Long.parseLong(buffer.readLine());

            this.movieService.removeMovie(id);
            this.rentalService.removeRentalsWithMovieID(id);

        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

//    // ---- Rentals ---- //

    public void addRental() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter client id: ");
            Long clientId = Long.parseLong(buffer.readLine());

            System.out.println("Enter movie id: ");
            Long movieId = Long.parseLong(buffer.readLine());
            this.rentalService.addRental(new Rental(clientId, movieId, new Date())).thenAcceptAsync(added -> {
                if (added)
                    System.out.println("Rental added successfully");
                else
                    System.out.println("Rental could not be added");
            });

        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    public void printRentals(){
        this.rentalService.getRentals().thenAcceptAsync(rentalList -> {
            System.out.println(" << Rentals >> ");
            rentalList.stream().sorted().forEach(System.out::println);
            System.out.println();
        });
    }

    public void printMostRentedMovie(){
        this.rentalService.getMostRentedMovieId().thenAcceptAsync(movieId ->
            this.movieService.getMovie(movieId).thenAcceptAsync(movie -> {
                System.out.println(" << Most Rented Movie >>");
                System.out.println(movie);
            })
        );
    }

    public void updateRental() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Updating rental -- ");

            System.out.println("Enter rental id: ");
            Long id = Long.parseLong(buffer.readLine());

            System.out.println("Enter rental's client id: ");
            Long clientId = Long.parseLong(buffer.readLine());

            System.out.println("Enter rental's movie id: ");
            Long movieId = Long.parseLong(buffer.readLine());

            System.out.println("Enter rental's date: ");
            Date date = new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(buffer.readLine());

            this.rentalService.updateRental(new Rental(id, clientId, movieId, date)).thenAcceptAsync(updated -> {
                if (updated)
                    System.out.println("Rental updated successfully");
                else
                    System.out.println("Rental could not be updated");
            });

        } catch(IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    public void removeRental() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Removing rental -- ");
            System.out.println("Enter rental id: ");
            Long id = Long.parseLong(buffer.readLine());

            this.rentalService.removeRental(id).thenAcceptAsync(removed -> {
                if (removed)
                    System.out.println("Rental removed successfully");
                else
                    System.out.println("Rental does not exist");
            });

        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }
}

