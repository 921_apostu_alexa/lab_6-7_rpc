package client.service;

import client.TcpClient;
import common.model.Movie;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;
import common.service.IMovieService;
import common.service.Message;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public class MovieService implements IMovieService {
    private final ExecutorService executorService;
    private final TcpClient tcpClient;

    public MovieService(ExecutorService executorService, TcpClient tcpClient) {
        this.executorService = executorService;
        this.tcpClient = tcpClient;
    }

    @Override
    public CompletableFuture<Boolean> addMovie(Movie movie) throws ValidatorException {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(ADD_MOVIE, Movie.MovieToString(movie));
            Message response = tcpClient.sendAndReceive(request);
            return response.getHeader().equals(Message.OK);
        }, this.executorService);
    }

    @Override
    public CompletableFuture<Movie> getMovie(Long ID) throws BaseException {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(GET_MOVIE, ID.toString());
            Message response = tcpClient.sendAndReceive(request);
            if (response.getHeader().equals(Message.OK))
                return Movie.StringToMovie(response.getBody());
            else
                throw new BaseException(response.getBody());
        }, this.executorService);
    }

    @Override
    public CompletableFuture<Boolean> updateMovie(Movie movie) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(UPDATE_MOVIE, Movie.MovieToString(movie));
            Message response = tcpClient.sendAndReceive(request);
            return response.getHeader().equals(Message.OK);
        }, this.executorService);
    }

    @Override
    public CompletableFuture<Boolean> removeMovie(Long ID) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(REMOVE_MOVIE, ID.toString());
            Message response = tcpClient.sendAndReceive(request);
            return response.getHeader().equals(Message.OK);
        }, this.executorService);
    }

    @Override
    public CompletableFuture<List<Movie>> getMovies() {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(GET_MOVIES, Message.EMPTY);
            Message response = tcpClient.sendAndReceive(request);
            if (response.getHeader().equals(Message.OK))
                return IMovieService.StringToList(response.getBody());
            else
                throw new BaseException(response.getBody());
        }, this.executorService);
    }

    @Override
    public CompletableFuture<List<Movie>> filterMoviesByYear(int year) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(FILTER_MOVIES_BY_YEAR, String.valueOf(year));
            Message response = tcpClient.sendAndReceive(request);
            if (response.getHeader().equals(Message.OK))
                return IMovieService.StringToList(response.getBody());
            else
                throw new BaseException(response.getBody());
        }, this.executorService);
    }
}
