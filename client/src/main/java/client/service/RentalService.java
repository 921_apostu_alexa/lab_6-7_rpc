package client.service;

import client.TcpClient;
import common.model.Rental;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;
import common.service.IRentalService;
import common.service.Message;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public class RentalService implements IRentalService {
    private final ExecutorService executorService;
    private final TcpClient tcpClient;

    public RentalService(ExecutorService executorService, TcpClient tcpClient) {
        this.executorService = executorService;
        this.tcpClient = tcpClient;
    }

    @Override
    public CompletableFuture<Boolean> addRental(Rental rental) throws ValidatorException {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(ADD_RENTAL, Rental.RentalToString(rental));
            Message response = tcpClient.sendAndReceive(request);
            return response.getHeader().equals(Message.OK);
        }, this.executorService);
    }

    @Override
    public CompletableFuture<Rental> getRental(Long ID) throws BaseException {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(GET_RENTALS, ID.toString());
            Message response = tcpClient.sendAndReceive(request);
            if(response.getHeader().equals(Message.OK))
                return Rental.StringToRental(response.getBody());
            else
                throw new BaseException(response.getBody());
        }, this.executorService);
    }

    @Override
    public CompletableFuture<Boolean> updateRental(Rental rental) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(UPDATE_RENTAL, Rental.RentalToString(rental));
            Message response = tcpClient.sendAndReceive(request);
            return response.getHeader().equals(Message.OK);
        }, this.executorService);
    }

    @Override
    public CompletableFuture<Boolean> removeRental(Long ID) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(REMOVE_RENTAL, ID.toString());
            Message response = tcpClient.sendAndReceive(request);
            return response.getHeader().equals(Message.OK);
        }, this.executorService);
    }

    @Override
    public CompletableFuture<List<Rental>> getRentals() {
       return CompletableFuture.supplyAsync( () ->{
        Message request = new Message(GET_RENTALS, Message.EMPTY);
        Message response = tcpClient.sendAndReceive(request);
        if (response.getHeader().equals(Message.OK))
            return IRentalService.StringToList(response.getBody());
        else
            throw new BaseException(response.getBody());
        }, this.executorService);
    }

    @Override
    public CompletableFuture<List<Rental>> filterRentalsByClient(Long clientID) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(FILTER_RENTAL_BY_CLIENT, String.valueOf(clientID));
            Message response = tcpClient.sendAndReceive(request);
            if (response.getHeader().equals(Message.OK))
                return IRentalService.StringToList(response.getBody());
            else
                throw new BaseException(response.getBody());
        }, this.executorService);

    }
    @Override
    public CompletableFuture<List<Rental>> filterRentalsByStartingDate(Date date) {
        return CompletableFuture.supplyAsync(() -> {
            String rentDate = new SimpleDateFormat("dd/MM/yyyy hh:mm").format(date);
            Message request = new Message(FILTER_RENTAL_BY_CLIENT, rentDate);
            Message response = tcpClient.sendAndReceive(request);
            if (response.getHeader().equals(Message.OK))
                return IRentalService.StringToList(response.getBody());
            else
                throw new BaseException(response.getBody());
        }, this.executorService);
    }
    @Override
    public CompletableFuture<Long> getMostRentedMovieId() {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(GET_MOST_RENTED_MOVIE_ID, Message.EMPTY);
            Message response = tcpClient.sendAndReceive(request);
            if(response.getHeader().equals(Message.OK))
                return Rental.StringToRental(response.getBody()).getMovieId();
            else
                throw new BaseException(response.getBody());
        }, this.executorService);

    }

    @Override
    public CompletableFuture<Boolean> removeRentalsWithClientID(Long id) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(REMOVE_RENTAL_WITH_CLIENT_ID, id.toString());
            Message response = tcpClient.sendAndReceive(request);
            return response.getHeader().equals(Message.OK);
        }, this.executorService);
    }

    @Override
    public CompletableFuture<Boolean> removeRentalsWithMovieID(Long id) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(REMOVE_RENTAL_WITH_MOVIE_ID, id.toString());
            Message response = tcpClient.sendAndReceive(request);
            return response.getHeader().equals(Message.OK);
        }, this.executorService);    }
}
