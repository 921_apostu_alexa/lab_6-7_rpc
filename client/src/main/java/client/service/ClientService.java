package client.service;

import client.TcpClient;
import common.model.Client;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;
import common.service.IClientService;
import common.service.Message;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

public class ClientService implements IClientService {
    private ExecutorService executorService;
    private TcpClient tcpClient;

    public ClientService(ExecutorService executorService, TcpClient tcpClient) {
        this.executorService = executorService;
        this.tcpClient = tcpClient;
    }

    @Override
    public CompletableFuture<Boolean> addClient(Client client) throws ValidatorException {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(ClientService.ADD_CLIENT, Client.ClientToString(client));
            Message response = tcpClient.sendAndReceive(request);
            return response.getHeader().equals(Message.OK);
        }, executorService);
    }

    @Override
    public CompletableFuture<Client> getClient(Long ID) throws BaseException {
        return null;
    }

    @Override
    public CompletableFuture<Boolean> updateClient(Client client) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(ClientService.UPDATE_CLIENT, Client.ClientToString(client));
            Message response = tcpClient.sendAndReceive(request);
            return response.getHeader().equals(Message.OK);
        }, executorService);
    }

    @Override
    public CompletableFuture<Boolean> removeClient(Long ID) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(ClientService.REMOVE_CLIENT, String.valueOf(ID));
            Message response = tcpClient.sendAndReceive(request);
            return response.getHeader().equals(Message.OK);
        }, executorService);
    }

    @Override
    public CompletableFuture<List<Client>> getClients() {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(ClientService.GET_CLIENTS, Message.EMPTY);
            Message response = tcpClient.sendAndReceive(request);
            return IClientService.StringToList(response.getBody());
        }, executorService);
    }

    @Override
    public CompletableFuture<List<Client>> filterClientsByName(String name) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(ClientService.FILTER_CLIENT_BY_NAME, name);
            Message response = tcpClient.sendAndReceive(request);
            return IClientService.StringToList(response.getBody());
        }, executorService);
    }
}
