package server.service;

import common.model.Movie;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;
import common.service.IMovieService;
import server.repository.sorting.SortingRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MovieService implements IMovieService {
    private final SortingRepository<Long, Movie> repo;
    private final ExecutorService executorService;

    public MovieService(SortingRepository<Long, Movie> repo, ExecutorService executorService) {
        this.repo = repo;
        this.executorService = executorService;
    }

    /**
     * Adds the given movie to the service's repository.
     *
     * @param movie must be a valid movie
     *
     * @throws ValidatorException if the movie is invalid
     * */
    @Override
    public CompletableFuture<Boolean> addMovie(Movie movie) throws ValidatorException {
        return CompletableFuture.supplyAsync(() -> !this.repo.save(movie).isPresent(), this.executorService);
    }

    /**
     * Returns the movie with the given {@code id}
     *
     * @param ID must not be null
     *
     * @return a {@code Movie} representing the movie with the given id.
     * @throws BaseException if the movie does not exist
     * */
    public CompletableFuture<Movie> getMovie(Long ID) throws BaseException {
        return CompletableFuture.supplyAsync(() -> {
            Optional<Movie> movieOptional = this.repo.findOne(ID);
            return movieOptional.orElseThrow(() -> new BaseException("No item with given id"));
        }, this.executorService);
    }

    /**
     * Updates the given movie
     *
     * @param movie must be a valid movie.
     * */
    public CompletableFuture<Boolean> updateMovie(Movie movie) {
        return CompletableFuture.supplyAsync(() -> !this.repo.update(movie).isPresent(), this.executorService);
    }

    /**
     * Removes the movie with the given {@code id}
     *
     * @param ID must not be null
     * */
    public CompletableFuture<Boolean> removeMovie(Long ID) {
        return CompletableFuture.supplyAsync(() -> this.repo.delete(ID).isPresent(), this.executorService);
    }

    /**
     * Returns all the movies from the repo
     *
     * @return a {@code Set}
     * */
    public CompletableFuture<List<Movie>> getMovies() {
        return CompletableFuture.supplyAsync(() ->
                StreamSupport.stream(this.repo.findAll().spliterator(), false)
                             .collect(Collectors.toList()),
                this.executorService);
    }

    /**
     * Returns only the movies from the repo released after given {@code year}
     *
     * @param year must not be null
     * @return a {@code Set}
     * */
    public CompletableFuture<List<Movie>> filterMoviesByYear(int year) {
        return CompletableFuture.supplyAsync(() ->
                StreamSupport.stream(this.repo.findAll().spliterator(), false)
                             .filter(movie -> movie.getYear() == year)
                             .collect(Collectors.toList()),
                this.executorService);
    }
}
