package server.service;

import common.model.BaseClass;
import common.model.Rental;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;
import common.service.IRentalService;
import server.repository.sorting.SortingRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class RentalService implements IRentalService {
    private SortingRepository<Long, Rental> repo;
    private ExecutorService executorService;

    /**
     * Creates the service with the given {@code Repository}
     * */
    public RentalService(SortingRepository<Long, Rental> repo, ExecutorService executorService) {
        this.executorService = executorService;
        this.repo = repo;
    }

    /**
     * Rents a movie from a client's perspective
     *
     * @param rental must not be null and the client with given id must exist
     * @throws ValidatorException if the rental is invalid
     * */
    public CompletableFuture<Boolean> addRental(Rental rental) throws ValidatorException {
        return CompletableFuture.supplyAsync(() -> {
            Date now = new Date();
            rental.setRentDate(now);
            return !this.repo.save(rental).isPresent();
        }, this.executorService);

    }

    /**
     * Returns the rental with the given {@code id}
     *
     * @param ID must not be null
     *
     * @return a {@code Rental} representing the rental with the given id.
     * @throws BaseException if the rental does not exist
     * */
    public CompletableFuture<Rental> getRental(Long ID) throws BaseException {
        return CompletableFuture.supplyAsync(() -> {
            Optional<Rental> rentalOptional = this.repo.findOne(ID);
            return rentalOptional.orElseThrow(() -> new BaseException("No item with given id"));
        }, this.executorService);

    }

    /**
     * Updates the given rental
     *
     * @param rental must be a valid rental.
     * */
    public CompletableFuture<Boolean> updateRental(Rental rental) {
        return CompletableFuture.supplyAsync(() -> !this.repo.update(rental).isPresent(), this.executorService);
    }

    /**
     * Removes the rental with the given {@code id}
     *
     * @param ID must not be null
     * */
    public CompletableFuture<Boolean> removeRental(Long ID) {
        return CompletableFuture.supplyAsync(() -> this.repo.delete(ID).isPresent(), this.executorService);
    }

    /**
     * Removes the rentals made by the client with the given id
     *
     * @param clientId must not be null and a client with that id must exist
     * */
    public CompletableFuture<Boolean> removeRentalsWithClientID(Long clientId) {
        return CompletableFuture.supplyAsync(() ->
                StreamSupport
                        .stream(this.repo.findAll().spliterator(), false)
                        .filter(rental -> rental.getClientId().equals(clientId))
                        .map(rental -> this.repo.delete(rental.getId()))
                        .map(optional -> !optional.isPresent())
                        .reduce(true, (acc, elem) -> acc && elem),
                this.executorService);
    }

    /**
     * Removes the rentals of the movie with the given id
     *
     * @param movieId must not be null and a movie with that id must exist
     * */
    public CompletableFuture<Boolean> removeRentalsWithMovieID(Long movieId) {
        return CompletableFuture.supplyAsync(() ->
                StreamSupport
                        .stream(this.repo.findAll().spliterator(), false)
                        .filter(rental -> rental.getMovieId().equals(movieId))
                        .map(rental -> this.repo.delete(rental.getId()))
                        .map(optional -> !optional.isPresent())
                        .reduce(true, (acc, elem) -> acc && elem),
                this.executorService);
    }

    /**
     * Returns all the rentals from the repo
     *
     * @return a {@code Set}
     * */
    public CompletableFuture<List<Rental>> getRentals() {
        return CompletableFuture.supplyAsync(() ->
                StreamSupport
                    .stream(this.repo.findAll().spliterator(), false)
                    .collect(Collectors.toList()),
                this.executorService);
    }

    /**
     * Returns only the rentals made by the client with given clientId
     *
     * @param clientID must not be null
     * @return a {@code Set}
     * */
    public CompletableFuture<List<Rental>> filterRentalsByClient(Long clientID) {
        return CompletableFuture.supplyAsync(() ->
                StreamSupport
                    .stream(this.repo.findAll().spliterator(), false)
                    .filter(rental -> rental.getClientId().equals(clientID))
                    .collect(Collectors.toList()),
                this.executorService);
    }

    /**
     * Returns only the rentals made after the given date
     *
     * @param date must not be null
     * @return a {@code Set}
     * */
    public CompletableFuture<List<Rental>> filterRentalsByStartingDate(Date date) {
        return CompletableFuture.supplyAsync(() ->
                StreamSupport
                    .stream(this.repo.findAll().spliterator(), false)
                    .filter(rental -> rental.getRentDate().after(date))
                    .collect(Collectors.toList()),
                this.executorService);
        }

    /**
     * Returns the id of the most rented movie
     *
     * @return a {@code Long}
     * */
    public CompletableFuture<Long> getMostRentedMovieId() {
        return CompletableFuture.supplyAsync(() ->
                StreamSupport
                    .stream(this.repo.findAll().spliterator(), false)
                    .collect(Collectors.groupingBy(Rental::getMovieId, Collectors.counting()))
                    .entrySet()
                    .stream()
                    .max((r1, r2) -> r1.getValue() > r2.getValue() ? 1 : 0)
                    .get()
                    .getKey(),
                this.executorService);
    }
}
