package server.service;

import common.model.Client;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;
import common.service.IClientService;
import server.repository.sorting.Sort;
import server.repository.sorting.SortingRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ClientService implements IClientService {
    private SortingRepository<Long, Client> repo;
    private ExecutorService executorService;

    /**
     * Creates the service with the given {@code Repository}
     */
    public ClientService(SortingRepository<Long, Client> repo, ExecutorService executorService) {
        this.executorService = executorService;
        this.repo = repo;
    }

    /**
     * Adds the given client to the service's repository.
     *
     * @param client must be a valid client
     * @throws ValidatorException if the client is invalid
     */
    public CompletableFuture<Boolean> addClient(Client client) throws ValidatorException {
        return CompletableFuture.supplyAsync(() -> !this.repo.save(client).isPresent(), this.executorService);
    }

    /**
     * Returns the client with the given {@code id}
     *
     * @param ID must not be null
     * @return a {@code Client} representing the client with the given id.
     * @throws BaseException if the client does not exist
     */
    public CompletableFuture<Client> getClient(Long ID) throws BaseException {
        return CompletableFuture.supplyAsync(() -> {
            Optional<Client> clientOptional = this.repo.findOne(ID);
            return clientOptional.orElseThrow(() -> new BaseException("No item with given id"));
        }, this.executorService);
    }

    /**
     * Updates the given client
     *
     * @param client must be a valid client.
     */
    public CompletableFuture<Boolean> updateClient(Client client) {
        return CompletableFuture.supplyAsync(() -> !this.repo.update(client).isPresent(), this.executorService);
    }

    /**
     * Removes the client with the given {@code id}
     *
     * @param ID must not be null
     */
    public CompletableFuture<Boolean> removeClient(Long ID) {
        return CompletableFuture.supplyAsync(() -> this.repo.delete(ID).isPresent(), this.executorService);
    }

    /**
     * Returns all the clients from the repo
     *
     * @return a {@code Set}
     */
    public CompletableFuture<List<Client>> getClients() {
        return CompletableFuture.supplyAsync(() -> {
            Sort sort1 = new Sort("name"); //sort asc by name

            return StreamSupport.stream(this.repo.findAll(sort1).spliterator(), false)
                    .collect(Collectors.toList());

        }, this.executorService);
    }

    /**
     * Returns only the clients from the repo that have the given {@code name}
     *
     * @param name must not be null
     * @return a {@code Set}
     */
    public CompletableFuture<List<Client>> filterClientsByName(String name) {
        return CompletableFuture.supplyAsync(() ->
                StreamSupport.stream(this.repo.findAll().spliterator(), false)
            .filter(client -> client.getName().contains(name))
            .collect(Collectors.toList()), this.executorService);
    }
}
