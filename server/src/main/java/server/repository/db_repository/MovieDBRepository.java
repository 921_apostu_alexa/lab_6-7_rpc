package server.repository.db_repository;

import common.model.Movie;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;
import common.model.validators.Validator;
import server.repository.sorting.Sort;
import server.repository.sorting.SortingRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MovieDBRepository implements SortingRepository<Long, Movie> {
    private static final String URL_CONNECTION = "jdbc:postgresql://localhost:5432/movie_rental";
    private static final String USER = System.getProperty("username");
    private static final String PASSWORD = System.getProperty("password");
    private Validator<Movie> validator;

    public MovieDBRepository(Validator<Movie> validator) {
        this.validator = validator;
    }

    @Override
    public Iterable<Movie> findAll() {
        try {
            String sql = "Select * from Movie";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            List<Movie> movies = new ArrayList<>();
            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String title = resultSet.getString("title");
                int minutes = resultSet.getInt("minutes");
                int year = resultSet.getInt("year");
                Movie movie = new Movie(id,title,minutes ,year);
                this.validator.validate(movie);

                movies.add(movie);
            }
            return movies;

        } catch (SQLException e) {
            throw new BaseException(e.getMessage());
        }
    }

    @Override
    public Iterable<Movie> findAll(Sort sort) {
        return null;
    }

    @Override
    public Optional<Movie> findOne(Long aLong) {
        try {
            String sql = "select * from movie where id=?";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            Long id = resultSet.getLong("id");
            String title = resultSet.getString("title");
            int minutes = resultSet.getInt("minutes");
            int year = resultSet.getInt("year");
            Movie movie = new Movie(id, title, minutes, year);
            this.validator.validate(movie);

            return Optional.of(movie);

        } catch (SQLException e) {
            throw new BaseException(e.getMessage());
        }
    }

    @Override
    public Optional<Movie> save(Movie entity) throws ValidatorException {
        try {
            this.validator.validate(entity);

            String sql = "insert into Movie (title, minutes, year) values (?, ?, ?)";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, entity.getTitle());
            statement.setInt(2, entity.getMinutes());
            statement.setInt(3, entity.getYear());
            statement.executeUpdate();

            return Optional.empty();

        } catch (SQLException e) {
//            return Optional.of(entity);
            throw new BaseException(e.getMessage());

        }
    }

    @Override
    public Optional<Movie> delete(Long aLong) {
        try {
            String getMovieSql = "select * from movie where id=?";
            String removeSql = "delete from movie where id=?";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);

            PreparedStatement getMovieStatement = connection.prepareStatement(getMovieSql);
            getMovieStatement.setLong(1, aLong);
            ResultSet resultSet = getMovieStatement.executeQuery();
            resultSet.next();
            Movie toRemoveMovie = new Movie(resultSet.getLong("id"),
                    resultSet.getString("title"),
                    resultSet.getInt("minutes"),
                    resultSet.getInt("year"));

            PreparedStatement statement = connection.prepareStatement(removeSql);
            statement.setLong(1, aLong);
            statement.executeUpdate();

            return Optional.of(toRemoveMovie);

        } catch (SQLException e) {
//            return Optional.empty();
            throw new BaseException(e.getMessage());

        }
    }

    @Override
    public Optional<Movie> update(Movie entity) throws ValidatorException {
        try {
            String sql = "update Movie set title=?, minutes=?, year=? where id=?";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, entity.getTitle());
            statement.setInt(2, entity.getMinutes());
            statement.setInt(3, entity.getYear());
            statement.setLong(4, entity.getId());
            statement.executeUpdate();

            return Optional.empty();

        } catch (SQLException e) {
//            return Optional.of(entity);
            throw new BaseException(e.getMessage());

        }
    }
}
