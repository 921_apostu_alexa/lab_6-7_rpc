package server.repository.db_repository;

import common.model.Rental;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;
import common.model.validators.Validator;
import server.repository.sorting.Sort;
import server.repository.sorting.SortingRepository;

import java.util.Date;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RentalDBRepository implements SortingRepository<Long, Rental> {
    private static final String URL_CONNECTION = "jdbc:postgresql://localhost:5432/movie_rental";
    private static final String USER = System.getProperty("username");
    private static final String PASSWORD = System.getProperty("password");
    private Validator<Rental> validator;

    public RentalDBRepository(Validator<Rental> validator) {
        this.validator = validator;
    }

    @Override
    public Optional<Rental> findOne(Long aLong) {
        try {
            String sql = "select * from rental where id=?";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            Long id         = resultSet.getLong("id");
            Long clientid   = resultSet.getLong("clientid");
            Long movieid    = resultSet.getLong("movieid");
            Date date       = resultSet.getDate("date");

            Rental rental = new Rental(id, clientid, movieid, date);
            this.validator.validate(rental);

            return Optional.of(rental);

        } catch (SQLException e) {
            throw new BaseException(e.getMessage());
        }
    }

    @Override
    public Iterable<Rental> findAll(Sort sort) {
        return null;
    }

    @Override
    public Iterable<Rental> findAll() {
        try {
            String sql = "Select * from rental";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            List<Rental> rentals = new ArrayList<>();
            while (resultSet.next()) {
                Long id         = resultSet.getLong("id");
                Long clientid   = resultSet.getLong("clientid");
                Long movieid    = resultSet.getLong("movieid");
                Date date       = resultSet.getDate("date");

                Rental rental = new Rental(id, clientid, movieid, date);
                this.validator.validate(rental);

                rentals.add(rental);
            }
            return rentals;

        } catch (SQLException e) {
            throw new BaseException(e.getMessage());
        }
    }

    @Override
    public Optional<Rental> save(Rental entity) throws ValidatorException {
        try {
            this.validator.validate(entity);

            String sql = "insert into rental (clientid, movieid, date) values (?, ?, ?)";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, entity.getClientId());
            statement.setLong(2, entity.getMovieId());
            statement.setDate(3, (java.sql.Date) entity.getRentDate());
            statement.executeUpdate();

            return Optional.empty();

        } catch (SQLException e) {
//            return Optional.of(entity);
            throw new BaseException(e.getMessage());
        }
    }

    @Override
    public Optional<Rental> delete(Long aLong) {
        try {
            String getRentalSql = "select * from rental where id=?";
            String removeSql = "delete from rental where id=?";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);

            PreparedStatement getRentalStatement = connection.prepareStatement(getRentalSql);
            getRentalStatement.setLong(1, aLong);
            ResultSet resultSet = getRentalStatement.executeQuery();
            resultSet.next();
            Rental toRemoveRental = new Rental(resultSet.getLong("id"),
                    resultSet.getLong("clientid"),
                    resultSet.getLong("movieid"),
                    resultSet.getDate("date"));

            PreparedStatement statement = connection.prepareStatement(removeSql);
            statement.setLong(1, aLong);
            statement.executeUpdate();

            return Optional.of(toRemoveRental);

        } catch (SQLException e) {
//            return Optional.empty();
            throw new BaseException(e.getMessage());
        }
    }

    @Override
    public Optional<Rental> update(Rental entity) throws ValidatorException {
        try {
            String sql = "update rental set clientid=?, movieid=?, date=? where id=?";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, entity.getClientId());
            statement.setLong(2, entity.getMovieId());
            statement.setDate(3, (java.sql.Date) entity.getRentDate());
            statement.executeUpdate();

            return Optional.empty();

        } catch (SQLException e) {
//            return Optional.of(entity);
            throw new BaseException(e.getMessage());
        }
    }
}
