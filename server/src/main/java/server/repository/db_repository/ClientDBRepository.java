package server.repository.db_repository;

import common.model.Client;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;
import common.model.validators.Validator;
import server.repository.sorting.Sort;
import server.repository.sorting.SortingRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static common.model.Client.ClientToString;

public class ClientDBRepository implements SortingRepository<Long, Client> {
    private static final String URL_CONNECTION = "jdbc:postgresql://localhost:5432/movie_rental";
    private static final String USER = System.getProperty("username");
    private static final String PASSWORD = System.getProperty("password");
    private Validator<Client> validator;

    public ClientDBRepository(Validator<Client> validator) {
        this.validator = validator;
    }

    @Override
    public Optional<Client> findOne(Long aLong) {
        try {
            String sql = "select * from client where id=?";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            Long id = resultSet.getLong("id");
            String name = resultSet.getString("name");
            String email = resultSet.getString("email");

            Client client = new Client(id, name, email);

            return Optional.of(client);

        } catch (SQLException e) {
            throw new BaseException(e.getMessage());
        }
    }


    @Override
    public Iterable<Client> findAll() {
        try {
            String sql = "Select * from client";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            List<Client> clients = new ArrayList<>();
            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String email = resultSet.getString("email");

                Client client = new Client(id, name, email);

                clients.add(client);
            }
            return clients;

        } catch (SQLException e) {
            throw new BaseException(e.getMessage());
        }
    }

    @Override
    public Iterable<Client> findAll(Sort sort) {
        return sort.apply(this.findAll());
    }

    @Override
    public Optional<Client> save(Client entity) throws ValidatorException {
        try {
            this.validator.validate(entity);

            String sql = "insert into client (name, email) values (?, ?)";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getEmail());
            statement.executeUpdate();

            return Optional.empty();

        } catch (SQLException e) {
//            return Optional.of(entity);
            throw new BaseException(e.getMessage());
        }
    }

    @Override
    public Optional<Client> delete(Long aLong) {
        try {
            String getClientSql = "select * from client where id=?";
            String removeSql = "delete from client where id=?";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);

            PreparedStatement getClientStatement = connection.prepareStatement(getClientSql);
            getClientStatement.setLong(1, aLong);
            ResultSet resultSet = getClientStatement.executeQuery();
            resultSet.next();
            Client toRemoveClient = new Client(resultSet.getLong("id"),
                                               resultSet.getString("name"),
                                               resultSet.getString("email"));

            PreparedStatement statement = connection.prepareStatement(removeSql);
            statement.setLong(1, aLong);
            statement.executeUpdate();

            return Optional.of(toRemoveClient);

        } catch (SQLException e) {
//            return Optional.empty();
            throw new BaseException(e.getMessage());
        }
    }

    @Override
    public Optional<Client> update(Client entity) throws ValidatorException {
        try {
            String sql = "update client set name=?, email=? where id=?";

            Connection connection = DriverManager.getConnection(URL_CONNECTION, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getEmail());
            statement.setLong(3, entity.getId());
            statement.executeUpdate();

            return Optional.empty();

        } catch (SQLException e) {
//            return Optional.of(entity);
            throw new BaseException(e.getMessage());
        }
    }
}
