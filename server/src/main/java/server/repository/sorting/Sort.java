package server.repository.sorting;

import javafx.util.Pair;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class Sort {
    public static final Boolean ASC  = Boolean.TRUE;
    public static final Boolean DESC = Boolean.FALSE;
    private List<Pair<String, Boolean>> sorts; // 0 - asc, 1 - desc


    public Sort(Boolean bool, String ... fields) {
        this.sorts = new ArrayList<>();
        Arrays.stream(fields).forEach(field -> sorts.add(new Pair<>(field, bool)));
    }

    public Sort(String ... fields) {
        this(ASC, fields);
    }

    public List<Pair<String, Boolean>> getAll() { return this.sorts; }

    public Sort and(Sort sort) {
        this.sorts.addAll(sort.getAll());
        return this;
    }

    public <T> Iterable<T> apply(Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false)
                .sorted(this::compare)
                .collect(Collectors.toList());
    }

    private <T> int compare(T o1, T o2) {
        return sorts.stream()
                .map(attribute -> compareByAttribute(o1, o2, attribute))
                .dropWhile(compareResult -> compareResult == 0)
                .findFirst().orElse(0);
    }

    private <T> Integer compareByAttribute(T o1, T o2, Pair<String, Boolean> attribute) {
        try {
            Class<?> cls = o1.getClass();
            Field field = cls.getDeclaredField(attribute.getKey());
            field.setAccessible(true);
            if (attribute.getValue() == ASC) {
                Comparable<T> a = (Comparable<T>) field.get(o1);
                T b = (T) field.get(o2);
                int out = a.compareTo(b);
                field.setAccessible(false);
                return out;
            }
            else {
                Comparable<T> b = (Comparable<T>) field.get(o2);
                T a = (T) field.get(o1);
                int out = b.compareTo(a);
                field.setAccessible(false);
                return out;
            }

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
