package server.repository.sorting;

import common.model.BaseClass;
import server.repository.IRepository;

import java.io.Serializable;

public interface SortingRepository
        <ID extends Serializable, T extends BaseClass<ID>> extends
        IRepository<ID, T> {

    Iterable<T> findAll(Sort sort);
}
