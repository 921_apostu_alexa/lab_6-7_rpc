package server;

import common.model.Client;
import common.model.Movie;
import common.model.Rental;
import common.model.validators.ClientValidator;
import common.model.validators.MovieValidator;
import common.model.validators.RentalValidator;
import common.model.validators.Validator;
import common.service.IClientService;
import common.service.IMovieService;
import common.service.IRentalService;
import common.service.Message;
import server.repository.db_repository.ClientDBRepository;
import server.repository.db_repository.MovieDBRepository;
import server.repository.db_repository.RentalDBRepository;
import server.repository.sorting.SortingRepository;
import server.service.ClientService;
import server.service.MovieService;
import server.service.RentalService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

public class MainServer {

    public static void main(String[] args) {
        try {
            System.out.println("Server boot up");

            ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

            Validator<Client> clientValidator = new ClientValidator();
            Validator<Movie> movieValidator   = new MovieValidator();
            Validator<Rental> rentalValidator = new RentalValidator();

            SortingRepository<Long, Client> clientDBRepository = new ClientDBRepository(clientValidator);
            SortingRepository<Long, Movie> movieDBRepository   = new MovieDBRepository(movieValidator);
            SortingRepository<Long, Rental> rentalDBRepository = new RentalDBRepository(rentalValidator);

            IClientService clientService = new ClientService(clientDBRepository, executorService);
            IMovieService movieService   = new MovieService(movieDBRepository, executorService);
            IRentalService rentalService = new RentalService(rentalDBRepository, executorService);

            TcpServer tcpServer = new TcpServer(executorService);

            setupHandlers(tcpServer, clientService, movieService, rentalService);

            tcpServer.startServer();

            executorService.shutdown();

            System.out.println("Server shut down");
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        }

    }

    private static void setupHandlers(TcpServer tcpServer, IClientService clientService,
                                      IMovieService movieService, IRentalService rentalService) {

        tcpServer.addHandler(IClientService.ADD_CLIENT, (request) -> {
            try {
                Client client = Client.StringToClient(request.getBody());
                Boolean addedClient = clientService.addClient(client).get();
                if (addedClient)
                    return new Message(Message.OK, "Added client: " + client.toString());
                else
                    return new Message(Message.ERROR, "Client with id: " + client.getId() + " already exists");
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IClientService.GET_CLIENTS, (__) -> {
            try {
                CompletableFuture<List<Client>> clientList = clientService.getClients();
                return new Message(Message.OK, IClientService.ListToString(clientList.get()));
            } catch (InterruptedException | ExecutionException e) {
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IClientService.UPDATE_CLIENT, (request) -> {
            try {
                Client client = Client.StringToClient(request.getBody());
                Boolean updatedClient = clientService.updateClient(client).get();
                if (updatedClient)
                    return new Message(Message.OK, "Updated client: " + client.toString());
                else
                    return new Message(Message.ERROR, "Client with id: " + client.getId() + " does not exist");
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IClientService.REMOVE_CLIENT, (request) -> {
            try {
                Long id = Long.parseLong(request.getBody());
                Boolean removedClient = clientService.removeClient(id).get();
                if(removedClient)
                    return new Message(Message.OK, "Remove client " + id + " ");
                else
                    return new Message(Message.ERROR, "Client with id " + id + " does not exist");
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IClientService.FILTER_CLIENT_BY_NAME, (request) -> {
            try {
                List<Client> clientList = clientService.filterClientsByName(request.getBody()).get();
                return new Message(Message.OK,  IClientService.ListToString(clientList) );
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });


        tcpServer.addHandler(IMovieService.ADD_MOVIE, (request) -> {
            try {
                Movie movie = Movie.StringToMovie(request.getBody());
                Boolean addedMovie = movieService.addMovie(movie).get();
                if (addedMovie)
                    return new Message(Message.OK, "Added movie: " + movie.toString());
                else
                    return new Message(Message.ERROR, "Movie " + movie.getTitle() + " already exists.");
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IMovieService.GET_MOVIE, (request) -> {
            try {
                Long id = Long.parseLong(request.getBody());
                CompletableFuture<Movie> movie = movieService.getMovie(id);
                return new Message(Message.OK, Movie.MovieToString(movie.get()));
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IMovieService.GET_MOVIES, (__) -> {
            try {
                CompletableFuture<List<Movie>> movieList = movieService.getMovies();
                return new Message(Message.OK, IMovieService.ListToString(movieList.get()));
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IMovieService.UPDATE_MOVIE, (request) -> {
            try {
                Movie movie = Movie.StringToMovie(request.getBody());
                Boolean updatedMovie = movieService.updateMovie(movie).get();
                if (updatedMovie)
                    return new Message(Message.OK, "Updated movie with id " + movie.getId());
                else
                    return new Message(Message.ERROR, "Movie with id " + movie.getId() + " does not exist");
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IMovieService.REMOVE_MOVIE, (request) -> {
            try {
                Long id = Long.parseLong(request.getBody());
                Boolean removedMovie = movieService.removeMovie(id).get();
                if (removedMovie)
                    return new Message(Message.OK, "Removed movie with id " + id);
                else
                    return new Message(Message.ERROR, "Movie with id " + id + " does not exist");
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IMovieService.FILTER_MOVIES_BY_YEAR, (request) -> {
            try {
                Integer year = Integer.parseInt(request.getBody());
                CompletableFuture<List<Movie>> movieList = movieService.filterMoviesByYear(year);
                return new Message(Message.OK, IMovieService.ListToString(movieList.get()));
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });


        tcpServer.addHandler(IRentalService.ADD_RENTAL, (request) -> {
            try {
                Rental rental = Rental.StringToRental(request.getBody());
                Boolean addedRental = rentalService.addRental(rental).get();
                if (addedRental)
                    return new Message(Message.OK, "Added rental: " + rental.toString());
                else
                    return new Message(Message.ERROR, "Rental with id " + rental.getId() + " already exists");
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IRentalService.GET_RENTALS, (__) -> {
            try {
                CompletableFuture<List<Rental>> rentalList = rentalService.getRentals();
                return new Message(Message.OK, IRentalService.ListToString(rentalList.get()));
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IRentalService.UPDATE_RENTAL, (request) -> {
            try {
                Rental rental = Rental.StringToRental(request.getBody());
                Boolean updatedRental = rentalService.updateRental(rental).get();
                if (updatedRental)
                    return new Message(Message.OK, "Updated rental: " + rental.toString());
                else
                    return new Message(Message.ERROR, "Rental with id: " + rental.getId() + " does not exist");
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IRentalService.REMOVE_RENTAL, (request) -> {
            try {
                Long id = Long.parseLong(request.getBody());
                Boolean removedRental = rentalService.removeRental(id).get();
                if (removedRental)
                    return new Message(Message.OK, "Removed rental with id " + id);
                else
                    return new Message(Message.ERROR, "Rental with id " + id + " does not exist");
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IRentalService.REMOVE_RENTAL_WITH_CLIENT_ID, (request) -> {
            try {
                Long clientID = Long.parseLong(request.getBody());
                Boolean removedRentals = rentalService.removeRentalsWithClientID(clientID).get();
                if (removedRentals)
                    return new Message(Message.OK, "Removed rentals with clientID = " + clientID);
                else
                    return new Message(Message.ERROR, "There is no rental with clientID = " + clientID);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IRentalService.REMOVE_RENTAL_WITH_MOVIE_ID, (request) -> {
            try {
                Long movieID = Long.parseLong(request.getBody());
                Boolean removedRentals = rentalService.removeRentalsWithClientID(movieID).get();
                if (removedRentals)
                    return new Message(Message.OK, "Removed rentals with movieID = " + movieID);
                else
                    return new Message(Message.ERROR, "There is no rental with movieID = " + movieID);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IRentalService.FILTER_RENTAL_BY_CLIENT, (request) -> {
            try {
                Long clientId = Long.parseLong(request.getBody());
                CompletableFuture<List<Rental>> rentalList = rentalService.filterRentalsByClient(clientId);
                return new Message(Message.OK, IRentalService.ListToString(rentalList.get()));
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });

        tcpServer.addHandler(IRentalService.FILTER_RENTAL_BY_STARTING_DATE, (request) -> {
            try {
                Date date = new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(request.getBody());
                CompletableFuture<List<Rental>> rentalList = rentalService.filterRentalsByStartingDate(date);
                return new Message(Message.OK, IRentalService.ListToString(rentalList.get()));
            } catch (InterruptedException | ExecutionException | ParseException e) {
                e.printStackTrace();
                return new Message(Message.OK, e.getMessage());
            }
        });

        tcpServer.addHandler(IRentalService.GET_MOST_RENTED_MOVIE_ID, (__) -> {
            try {
                CompletableFuture<Long> rentalId = rentalService.getMostRentedMovieId();
                return new Message(Message.OK, rentalId.get().toString());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return new Message(Message.ERROR, e.getMessage());
            }
        });
    }
}
