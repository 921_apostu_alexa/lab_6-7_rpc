package common.model.validators;

import common.model.Rental;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;

import java.util.Optional;

public class RentalValidator implements Validator<Rental> {
    @Override
    public void validate(Rental entity) throws ValidatorException {
        Optional.ofNullable(entity.getId()).orElseThrow(() -> new BaseException("ID must not be null"));
        Optional.ofNullable(entity.getRentDate()).orElseThrow(() -> new BaseException("Rent date must not be null"));
    }
}
