package common.model;

import java.util.Arrays;
import java.util.List;

public class Movie extends BaseClass<Long> implements Comparable<Movie> {
    private String title;
    private int minutes;
    private int year;
    private static long currId = 0;

    public Movie(Long id, String title, int minutes, int year) {
        this.setId(id);
        this.title = title;
        this.minutes = minutes;
        this.year = year;
    }

    public Movie(String title, int minutes, int year) {
        super.setId(++currId);
        this.title = title;
        this.minutes = minutes;
        this.year = year;
    }

    public static Movie StringToMovie(String params) {
        List<String> items = Arrays.asList(params.split(","));

        Long id = Long.parseLong(items.get(0));
        String title = items.get(1);
        Integer minutes = Integer.parseInt(items.get(2));
        Integer year = Integer.parseInt(items.get(3));

        return new Movie(id, title, minutes, year);
    }

    public static String MovieToString(Movie movie) {
        return "" + movie.getId() + "," + movie.getTitle() + "," +
                movie.getMinutes() + "," + movie.getYear();
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
        currId = Math.max(currId, id);
    }

    public String getTitle() {
        return title;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getYear() {
        return year;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return getId() + ": " + getTitle() + " (" + getYear() + ")";
    }

    @Override
    public int compareTo(Movie movie) {
        return this.getId().compareTo(movie.getId());
    }
}
