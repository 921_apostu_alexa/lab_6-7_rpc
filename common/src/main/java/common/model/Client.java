package common.model;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;

public class Client extends BaseClass<Long> implements Comparable<Client> {
    private String name;
    private String email;
    public static long currId = 0;

    public Client(){

    }

    public Client(Long id, String name, String email) {
        this.setId(id);
        this.name = name;
        this.email = email;
    }

    public Client(String name, String email) {
        super.setId(++currId);
        this.name = name;
        this.email = email;
    }

    public static Client StringToClient(String params){
//        1,Florin,florin@salam.com
        List<String> items = Arrays.asList(params.split(","));

        Long id = Long.parseLong(items.get(0));
        String name = items.get(1);
        String email = items.get(2);

        return new Client(id, name, email);
    }

    public static String ClientToString(Client client){
        return "" + client.getId() + "," + client.getName() + "," + client.getEmail();
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
        currId = Math.max(currId, id);
    }

    public String getName() {
        return name;
    }

    public void setEmail(String email) { this.email = email; }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    @Override
    public String toString() {
        return getId() + ": " + getName() + " " + getEmail();
    }

    @Override
    public int compareTo(Client client) {
        return this.getId().compareTo(client.getId());
    }
}
