package common.model;

import common.model.exceptions.BaseException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Rental extends BaseClass<Long> implements Comparable<Rental> {
    private Long clientId;
    private Long movieId;
    private Date rentDate;  // for 2 weeks
    private static long rentId = 0;

    public Rental(Long id, Long clientId, Long movieId, Date rentDate) {
        this.setId(id);
        this.clientId = clientId;
        this.movieId = movieId;
        this.rentDate = rentDate;
    }

    public Rental(Long clientId, Long movieId, Date rentDate) {
        super.setId(++rentId);
        this.clientId = clientId;
        this.movieId = movieId;
        this.rentDate = rentDate;
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
        rentId = Math.max(rentId, id);
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public Date getRentDate() {
        return rentDate;
    }

    public void setRentDate(Date rentDate) {
        this.rentDate = rentDate;
    }

    @Override
    public String toString() {
        return getId() + ": Client " + getClientId() + " rented Movie " + getMovieId() + " on " + getRentDate().toString();

    }

    public static String RentalToString(Rental rental){
        return "" + rental.getId() + "," + rental.getClientId() + "," + rental.getMovieId() +
                "," + new SimpleDateFormat("dd/MM/yyyy hh:mm").format(rental.getRentDate());
    }
    public static Rental StringToRental(String params){
        List<String> items = Arrays.asList(params.split(","));

        Long id = Long.parseLong(items.get(0));
        Long clientId = Long.parseLong(items.get(1));
        Long movieId = Long.parseLong(items.get(2));
        Date date;

        try {
            date = new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(items.get(3));
        } catch (ParseException e) {
            throw new BaseException("RentalFileRepo - " + e.getMessage());
        }

        Rental rental = new Rental(clientId, movieId, date);
        rental.setId(id);
        return rental;
    }



    @Override
    public int compareTo(Rental rental) {
        return this.getId().compareTo(rental.getId());
    }
}
