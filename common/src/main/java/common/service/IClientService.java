package common.service;

import common.model.Client;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public interface IClientService {
    String ADD_CLIENT = "addClient";
    String GET_CLIENTS = "getClients";
    String REMOVE_CLIENT = "removeClient";
    String UPDATE_CLIENT = "updateClient";
    String FILTER_CLIENT_BY_NAME = "filterClientsByName";

    /**
     * Adds the given client to the service's repository.
     *
     * @param client must be a valid client
     * @throws ValidatorException if the client is invalid
     */
    CompletableFuture<Boolean> addClient(Client client) throws ValidatorException;

    /**
     * Returns the client with the given {@code id}
     *
     * @param ID must not be null
     * @return a {@code Client} representing the client with the given id.
     * @throws BaseException if the client does not exist
     */
    CompletableFuture<Client> getClient(Long ID) throws BaseException;

    /**
     * Updates the given client
     *
     * @param client must be a valid client.
     */
    CompletableFuture<Boolean> updateClient(Client client);

    /**
     * Removes the client with the given {@code id}
     *
     * @param ID must not be null
     */
    CompletableFuture<Boolean> removeClient(Long ID);

    /**
     * Returns all the clients from the repo
     *
     * @return a {@code Set}
     */
    CompletableFuture<List<Client>> getClients();

    /**
     * Returns only the clients from the repo that have the given {@code name}
     *
     * @param name must not be null
     * @return a {@code Set}
     */
    CompletableFuture<List<Client>> filterClientsByName(String name);

    static String ListToString(List<Client> clientList) {
        return clientList.stream()
                .map(Client::ClientToString)
                .reduce( "", (acc, elem) -> acc + elem + ";" );
    }

    static List<Client> StringToList(String wannaBeListOfClients) {
        return Arrays.stream(wannaBeListOfClients.split(";"))
                .map(Client::StringToClient)
                .collect(Collectors.toList());

    }
}
