package common.service;

import common.model.Client;
import common.model.Movie;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public interface IMovieService {
    String ADD_MOVIE = "addMovie";
    String GET_MOVIE = "getMovie";
    String UPDATE_MOVIE = "updateMovie";
    String REMOVE_MOVIE = "removeMovie";
    String GET_MOVIES = "getMovies";
    String FILTER_MOVIES_BY_YEAR = "filterMoviesByYear";

    /**
     * Adds the given movie to the service's repository.
     *
     * @param movie must be a valid movie
     *
     * @throws ValidatorException if the movie is invalid
     * */
    CompletableFuture<Boolean> addMovie(Movie movie) throws ValidatorException;

    /**
     * Returns the movie with the given {@code id}
     *
     * @param ID must not be null
     *
     * @return a {@code Movie} representing the movie with the given id.
     * @throws BaseException if the movie does not exist
     * */
    CompletableFuture<Movie> getMovie(Long ID) throws BaseException;

    /**
     * Updates the given movie
     *
     * @param movie must be a valid movie.
     * */
    CompletableFuture<Boolean> updateMovie(Movie movie);

    /**
     * Removes the movie with the given {@code id}
     *
     * @param ID must not be null
     * */
    CompletableFuture<Boolean> removeMovie(Long ID);

    /**
     * Returns all the movies from the repo
     *
     * @return a {@code Set}
     * */
    CompletableFuture<List<Movie>> getMovies();

    /**
     * Returns only the movies from the repo released after given {@code year}
     *
     * @param year must not be null
     * @return a {@code Set}
     * */
    CompletableFuture<List<Movie>> filterMoviesByYear(int year);

    static String ListToString(List<Movie> movieList) {
        return movieList.stream()
                .map(Movie::MovieToString)
                .reduce( "", (acc, elem) -> acc + elem + ";" );
    }

    static List<Movie> StringToList(String wannaBeListOfMovies) {
        return Arrays.stream(wannaBeListOfMovies.split(";"))
                .map(Movie::StringToMovie)
                .collect(Collectors.toList());

    }
}
