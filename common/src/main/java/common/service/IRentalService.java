package common.service;

import common.model.Rental;
import common.model.Rental;
import common.model.exceptions.BaseException;
import common.model.exceptions.ValidatorException;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public interface IRentalService {
    String ADD_RENTAL = "addRental";
    String GET_RENTALS = "getRentals";
    String GET_MOST_RENTED_MOVIE_ID = "getMostRentedMovieId";
    String REMOVE_RENTAL = "removeRental";
    String REMOVE_RENTAL_WITH_CLIENT_ID = "removeRentalsWithClientID";
    String REMOVE_RENTAL_WITH_MOVIE_ID = "removeRentalsWithMovieID";
    String UPDATE_RENTAL = "updateRental";
    String FILTER_RENTAL_BY_CLIENT = "filterRentalsByClient";
    String FILTER_RENTAL_BY_STARTING_DATE = "filterRentalsByStartingDate";

    /**
     * Adds the given rental to the service's repository.
     *
     * @param rental must be a valid rental
     * @throws ValidatorException if the rental is invalid
     */
    CompletableFuture<Boolean> addRental(Rental rental) throws ValidatorException;

    /**
     * Returns the rental with the given {@code id}
     *
     * @param ID must not be null
     * @return a {@code Rental} representing the rental with the given id.
     * @throws BaseException if the rental does not exist
     */
    CompletableFuture<Rental> getRental(Long ID) throws BaseException;

    /**
     * Updates the given rental
     *
     * @param rental must be a valid rental.
     */
    CompletableFuture<Boolean> updateRental(Rental rental);

    /**
     * Removes the rental with the given {@code id}
     *
     * @param ID must not be null
     */
    CompletableFuture<Boolean> removeRental(Long ID);

    /**
     * Returns all the rentals from the repo
     *
     * @return a {@code Set}
     */
    CompletableFuture<List<Rental>> getRentals();


    static String ListToString(List<Rental> rentalList) {
        return rentalList.stream()
                .map(Rental::RentalToString)
                .reduce("", (acc, elem) -> acc + elem + ";");
    }

    static List<Rental> StringToList(String wannaBeListOfRentals) {
        return Arrays.stream(wannaBeListOfRentals.split(";"))
                .map(Rental::StringToRental)
                .collect(Collectors.toList());

    }

    CompletableFuture<List<Rental>> filterRentalsByClient(Long clientID);

    /**
     * Returns only the rentals made after the given date
     *
     * @param date must not be null
     * @return a {@code Set}
     */
    CompletableFuture<List<Rental>> filterRentalsByStartingDate(Date date);

    /**
     * Returns the id of the most rented movie
     *
     * @return a {@code Long}
     */
    CompletableFuture<Long> getMostRentedMovieId();

    CompletableFuture<Boolean> removeRentalsWithClientID(Long id);

    CompletableFuture<Boolean> removeRentalsWithMovieID(Long id);
}
